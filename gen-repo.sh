#!/bin/bash --
# -*- mode:shell-script; coding:utf-8; tab-width:4 -*-

BASE=.
REPO=mp3player

function repo-init() {
	hg init $REPO
}

function repo-commit() {
	echo "---:: Commiting" $1
    ( cd $REPO && ant clean > /dev/null )
    ( cd $BASE/$1 && ant clean > /dev/null )
	cp -r $BASE/$1/. $REPO > /dev/null
	hg -R $REPO add
	hg -R $REPO revert $REPO/libs/android-support-v4.jar
    hg -R $REPO ci -m "$2"
}

make clean
bucket del DavidVilla/mp3player; bucket create DavidVilla/mp3player
rm -rf mp3player/
repo-init
repo-commit 00 "hello world"
repo-commit 01 "action_exit"
repo-commit 02 "play_button"
repo-commit 03 "play hardcoded mp3"
repo-commit 04 "show hardcoded song list"
repo-commit 05 "list actual songs: send Intent"
repo-commit 06 "select a song: activity result"
repo-commit 07 "song title"
repo-commit 08 "refactor MediaManager -> SongData"
repo-commit 09 "FileFilter for directories"
repo-commit 10 "play/pause button"
repo-commit 11 "prev and next buttons"
repo-commit 12 "title and album from metadata"
repo-commit 13 "cover image from metadata"
repo-commit 14 "playSong uses playlist now"
repo-commit 15 "SongListActivity returns an array"
repo-commit 16 "custom layout for SongListActivity rows: ChechedTextView"
repo-commit 17 "add and cancel buttons in SongListActivity"
repo-commit 18 "PlayList store with SQLiteDatabase"

cp hgrc $REPO/.hg/
hg -R $REPO push

echo "-- DONE --"
