#!/usr/bin/make -f
# -*- mode:makefile -*-

STEPS = $(shell seq -w 00 18)

clean:   CMD = ant clean > /dev/null
release: CMD = ant release > /dev/null

release clean: steps

.PHONY: steps $(STEPS)
steps: $(STEPS)
$(STEPS):
	cd $@ && $(CMD)
